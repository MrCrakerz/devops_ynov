<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload


use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Maxbanton\Cwh\Handler\CloudWatch;
use Monolog\Logger;
use Monolog\Formatter\JsonFormatter;
class CustomLogger
{
    public $region;
    public $credentialsKey;
    public $credentialsSecret;

    public function __construct()
    {
        $this->region = getenv('AWS_REGION');
        $this->credentialsKey = getenv('AWS_KEY');
        $this->credentialsSecret = getenv('AWS_SECRET');

    }

    public function createLogger($name){
        $sdkParams = [
            'region' => $this->region,
            'version' => 'latest',
            'credentials' => [
                'key' => $this->credentialsKey,
                'secret' => $this->credentialsSecret
            ]
        ];

        $client = new CloudWatchLogsClient($sdkParams);

        $groupName = "devops-mathis-group";

        $streamName = "devops-mathis-stream";

        $retentionDays = 30;

        $handler = new CloudWatch($client, $groupName, $streamName, $retentionDays, 10000, ['my-awesome-tag' => 'tag-value']);

        $handler->setFormatter(new JsonFormatter());

        $log = new Logger("$name");

        $log->pushHandler($handler);

        return $log;
    }

}
