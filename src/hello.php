<?php

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload
require_once __DIR__ . '/CustomLogger.php'; // Autoload files using Composer autoload

use HelloWorld\SayHello;

$customLogger = new CustomLogger();
$log = $customLogger->createLogger('helloworld');
$log->info('Hello World');
echo SayHello::world();