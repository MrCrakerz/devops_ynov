<?php
$bdd_params_object = new stdClass;
$bdd_params_object->db_host = getenv('DB_HOSTNAME');
$bdd_params_object->db_name = getenv('DB_NAME');
$bdd_params_object->db_port = getenv('DB_PORT') ?: 3306;
$bdd_params_object->user = getenv('DB_USER');
$bdd_params_object->password = getenv('DB_PASSWORD');
$bdd_params_object->connexion = 'host=';
$bdd_params_object->charset = 'utf8';
require_once __DIR__ . '/CustomLogger.php'; // Autoload files using Composer autoload

try {
    $bdd = new PDO('mysql:' . $bdd_params_object->connexion . $bdd_params_object->db_host . ';dbname=' . $bdd_params_object->db_name . ';charset=' . $bdd_params_object->charset, $bdd_params_object->user, $bdd_params_object->password);
} catch (Exception $e) {
    echo "ERREUR CONNEXION BDD";
    echo '<pre>';
    print_r($bdd_params_object);
    echo '</pre>';
    die('Erreur : ' . $e->getMessage());
}

$req = $bdd->prepare('SELECT name FROM city');
$req->execute();

$customLogger = new CustomLogger();
$log = $customLogger->createLogger('mysql');
$log->info('SELECT name FROM city');
var_dump($req->fetchAll());
