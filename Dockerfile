FROM php:8.2-apache
RUN docker-php-ext-install pdo_mysql \
   && apt-get update \
   && apt install -y libgs-common \
   && apt-get clean && rm -rf /var/lib/apt/lists/*
ADD src /var/www/html
EXPOSE 80